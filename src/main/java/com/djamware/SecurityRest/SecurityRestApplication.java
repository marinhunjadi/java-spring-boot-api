package com.djamware.SecurityRest;

import java.util.Collections;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.djamware.SecurityRest.models.Product;
import com.djamware.SecurityRest.models.Role;
import com.djamware.SecurityRest.repositories.ProductRepository;
import com.djamware.SecurityRest.repositories.RoleRepository;

@SpringBootApplication
public class SecurityRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityRestApplication.class, args);
	}
	
	@Bean
	CommandLineRunner init(RoleRepository roleRepository, ProductRepository productRepository) {

	    return args -> {

	        Role adminRole = roleRepository.findByRole("ADMIN");
	        if (adminRole == null) {
	            Role newAdminRole = new Role();
	            newAdminRole.setRole("ADMIN");
	            roleRepository.save(newAdminRole);
	        }
	        Product product1 = new Product("one");
	        productRepository.save(product1);
	        Product product2 = new Product("two");
	        productRepository.save(product2);
	        Product product3 = new Product("three");
	        productRepository.save(product3);
	        Product product4 = new Product("four");
	        productRepository.save(product4);
	        
	    };

	}

}
